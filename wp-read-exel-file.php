<?php
/**
 * Plugin Name: WP EXEL FILE READER.
 * Description: CONVERT EXEL FILE DATA TO ARRAY.
 * Version: 0.1
 */

/**
 * Create plugin page settings.
 */
function add_wpefr_page() {
	//add_submenu_page( 'edit.php?post_type=es_subscriber', __( 'Settings', 'cs' ), __( 'Settings', 'cs' ), 10, __FILE__, 'cs_admin_page' );

	add_menu_page( __( 'Options WP EXEL FILE READER', 'wprat' ), __( 'Options WP EXEL FILE READER', 'wprat' ), 'manage_options', 'wpefr_options' );
	add_submenu_page( 'wpefr_options', __( 'WP EXEL FILE READER', 'wprat' ), __( 'WP EXEL FILE READER', 'wprat' ), 'manage_options', 'wpefr_options', 'wpefr_options_page_output' );
}

add_action( 'admin_menu', 'add_wpefr_page' );

/**
 * Output content.
 */
function wpefr_options_page_output() {
	?>
	<div class="wrap">
		<form action="options.php" method="POST">
			<?php
			settings_fields( 'wpefr_options_group' );
			do_settings_sections( 'wpefr_options' );
			submit_button();
			?>
		</form>
	</div>
	<?php
}

/**
 * Registering settings.
 */
function wpefr_settings() {
	register_setting( 'wpefr_options_group', 'wpefr_options_name', array(
		'sanitize_callback' => 'wpefr_options_callback',
	) );

	add_settings_section( 'section_options', __( 'Main options WP Exel File Read Plugin', 'wprat' ), '', 'wpefr_options' );
	add_settings_field( 'wpefr_file_name', __( 'File name', 'wprat' ), 'fill_file_name', 'wpefr_options', 'section_options' );

}

add_action( 'admin_init', 'wpefr_settings' );
/**
 * Output data Main Option.
 */
function fill_file_name() {
	$val   = get_option( 'wpefr_options_name' );
	$val_a = isset( $val ) ? $val['file_name'] : NULL;
	?>
	<table>
		<tr>
			<th scope="row"><?php esc_html_e( 'File name: ', 'wprat' ); ?>
			</th>
			<td><input type="text" placeholder="example.com"
			           name="wpefr_options_name[file_name]"
			           value="<?php echo esc_attr( $val_a ); ?>"/>
				<p class="description"><?php esc_html_e( 'Your file name' ) ?></p>
			</td>
		</tr>
	</table>
	<?php
}

/**
 * Sanitize Main option page callback.
 */
function wpefr_options_callback( $options ) {
	foreach ( $options as $name => &$val ) {
		if ( 'file_name' === $name ) {
			$val = sanitize_text_field( $val );
		}
	}

	return $options;
}

/**
 * Read data from any excel file and create array.
 * $filename (string) path to the file from server's root.
 *
 * @param $filename
 *
 * @return array
 * @throws \PHPExcel_Reader_Exception
 */
function parse_excel_file( $filename ) {
	require_once dirname( __FILE__ ) . '/PHPExcel/Classes/PHPExcel.php';

	$result = array();
	$path   = ABSPATH . 'wp-content/uploads/' . $filename;

	$file_type   = PHPExcel_IOFactory::identify( $path );
	$objReader   = PHPExcel_IOFactory::createReader( $file_type );
	$objPHPExcel = $objReader->load( $path );
	$result      = $objPHPExcel->getActiveSheet()->toArray();

	return $result;
}

/**
 * @return array
 */
function get_array_from_array() {
	$file = get_option( 'wpefr_options_name' );

	if ( ! file_exists( ABSPATH . 'wp-content/uploads/' . $file['file_name'] ) ) {
		return [ ];
	}
	$res = parse_excel_file( $file['file_name'] );

	return $res;
}
